package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.i18n.KeyedMessage;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.CommandCanceledException;
import com.atlassian.bitbucket.scm.git.GitScm;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.throttle.ResourceBusyException;
import com.atlassian.bitbucket.throttle.ThrottleService;
import com.atlassian.bitbucket.throttle.Ticket;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.time.Duration;
import java.util.List;

import static com.atlassian.bitbucket.internal.unapprove.GitRescopeAnalyzer.RESOURCE;
import static com.atlassian.bitbucket.internal.unapprove.PullRequestTestHelper.mockPullRequest;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GitRescopeAnalyzerMockTest {

    private static final Duration PATCH_ID_TIMEOUT = Duration.ofSeconds(10);

    private GitRescopeAnalyzer analyzer;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private GitCommandBuilderFactory builderFactory;
    @Mock
    private Repository repository;
    @Mock
    private ThrottleService throttleService;
    @Mock
    private Ticket ticket;
    @Mock
    private AutoUnapproveConfig unapproveConfig;

    @Before
    public void setup() {
        when(repository.getScmId()).thenReturn(GitScm.ID);
        when(throttleService.acquireTicket(eq(RESOURCE))).thenReturn(ticket);
        when(unapproveConfig.isPatchIdEnabled()).thenReturn(true);
        when(unapproveConfig.getPatchIdTimeout()).thenReturn(PATCH_ID_TIMEOUT);

        analyzer = new GitRescopeAnalyzer(builderFactory, throttleService, unapproveConfig);
    }

    @Test
    public void testIsUpdatedCommandCancelled() {
        when(builderFactory.builder(repository).command("diff"))
                .thenThrow(new CommandCanceledException(new KeyedMessage("", "", "")));

        boolean updated = analyzer.isUpdated(mockPullRequest(repository, "currentFrom", "currentTo"), "previousFrom");

        assertTrue(updated);
        verify(builderFactory.builder(repository)).command("diff");
        verify(builderFactory.builder(repository), never()).command("patch-id");
    }

    @Test
    public void testIsUpdatedForHgRepository() {
        when(repository.getScmId()).thenReturn("hg");

        assertTrue(analyzer.isUpdated(mockPullRequest(repository, "currentFrom", "currentTo"), "previousFrom"));

        // Git processes should not be run for repositories from other SCMs
        verifyZeroInteractions(builderFactory, throttleService, ticket);
    }

    @Test
    public void testIsUpdatedResourceBusy() {
        when(throttleService.acquireTicket(RESOURCE))
                .thenThrow(new ResourceBusyException(new KeyedMessage("", "", ""), RESOURCE));
        assertTrue(analyzer.isUpdated(mockPullRequest(repository, "currentFrom", "currentTo"), "previousFrom"));

        verifyZeroInteractions(builderFactory);
    }

    @Test
    public void testIsUpdatedWithPatchId() {
        String toHash = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
        String fromHash = "cccccccccccccccccccccccccccccccccccccccc";
        String initialFromHash = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        PullRequest pullRequest = mockPullRequest(repository, initialFromHash, toHash);
        GitCommand diffCommand = mock(GitCommand.class);
        when(builderFactory.builder(same(repository))
                .command(eq("diff"))
                .argument(eq("--binary"))
                .argument(eq("--full-index"))
                .argument(eq(toHash + "..." + fromHash))
                .argument(eq("--"))
                .build(any()))
                .thenReturn(diffCommand);
        File file = mock(File.class);
        when(diffCommand.call()).thenReturn(file);
        GitCommand<List<String>> patchIdsCommand = mock(GitCommand.class);
        when(builderFactory.builder(repository)
                .command(eq("patch-id"))
                .argument("--stable")
                .inputHandler(argThat((ArgumentMatcher<GitRescopeAnalyzer.FileInputHandler>) argument ->
                        file.equals(argument.getFile())))
                .build(any(GitRescopeAnalyzer.PatchIdOutputHandler.class))).thenReturn(patchIdsCommand);

        assertTrue(analyzer.isUpdated(pullRequest, fromHash));
        verify(builderFactory.builder(repository)).command("diff");
        verify(diffCommand).setTimeout(PATCH_ID_TIMEOUT);
        verify(diffCommand).call();
        verify(builderFactory.builder(repository)).command("patch-id");
        verify(patchIdsCommand).setTimeout(PATCH_ID_TIMEOUT);
        verify(patchIdsCommand).call();
    }

    @Test
    public void testIsUpdatedWithPatchIdNoDiff() {
        String toHash = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
        String fromHash = "cccccccccccccccccccccccccccccccccccccccc";
        String initialFromHash = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        PullRequest pullRequest = mockPullRequest(repository, initialFromHash, toHash);
        GitCommand diffCommand = mock(GitCommand.class);
        when(builderFactory.builder(same(repository))
                .command(eq("diff"))
                .argument(eq("--binary"))
                .argument(eq("--full-index"))
                .argument(eq(toHash + "..." + fromHash))
                .argument(eq("--"))
                .build(any()))
                .thenReturn(diffCommand);
        when(diffCommand.call()).thenReturn(null); // no diff

        assertTrue(analyzer.isUpdated(pullRequest, fromHash));
        verify(diffCommand).setTimeout(PATCH_ID_TIMEOUT);
        verify(diffCommand).call();
        verify(builderFactory.builder(repository)).command("diff");
        verify(builderFactory.builder(repository), never()).command("patch-id");
    }

    @Test
    public void testIsUpdatedWithUnchangedFromCommit() {
        PullRequest pullRequest = mockPullRequest(repository, "currentFrom", "currentTo");
        assertFalse(analyzer.isUpdated(pullRequest, "currentFrom"));

        // Git processes should not be run when the "fromRef" commit is not updated
        verifyZeroInteractions(builderFactory, throttleService, ticket);
    }

    @Test
    public void testIsUpdatedWithoutPatchId() {
        when(unapproveConfig.isPatchIdEnabled()).thenReturn(false);

        PullRequest pullRequest = mockPullRequest(repository, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");

        assertTrue(analyzer.isUpdated(pullRequest, "cccccccccccccccccccccccccccccccccccccccc"));

        verify(repository, never()).getScmId();
        verifyZeroInteractions(throttleService);
    }
}
