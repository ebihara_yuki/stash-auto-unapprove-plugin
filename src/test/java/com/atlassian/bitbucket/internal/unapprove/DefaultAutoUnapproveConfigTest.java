package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.junit.TemporaryDirectory;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.server.StorageService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.time.Duration;

import static com.atlassian.bitbucket.internal.unapprove.DefaultAutoUnapproveConfig.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultAutoUnapproveConfigTest {

    @Rule
    public final TemporaryDirectory folder = new TemporaryDirectory();
    @Mock
    private ApplicationPropertiesService applicationPropertiesService;
    private DefaultAutoUnapproveConfig config;
    @Mock
    private StorageService storageService;

    @Before
    public void setup() {
        when(storageService.getTempDir()).thenReturn(folder.getRoot());

        config = new DefaultAutoUnapproveConfig(applicationPropertiesService, storageService);
    }

    @Test
    public void testIsGetPatchIdEnabled() {
        when(applicationPropertiesService.getPluginProperty(PROP_PATCH_ID_TIMEOUT_SECONDS, PATCH_ID_TIMEOUT_DEFAULT))
                .thenReturn(12L);
        when(applicationPropertiesService.getPluginProperty(PROP_PATCH_ID_ENABLED, PATCH_ID_ENABLED_DEFAULT))
                .thenReturn(true);
        config = new DefaultAutoUnapproveConfig(applicationPropertiesService, storageService);

        assertTrue(config.isPatchIdEnabled());
    }

    @Test
    public void testGetPatchIdTimeout() {
        when(applicationPropertiesService.getPluginProperty(PROP_PATCH_ID_TIMEOUT_SECONDS, PATCH_ID_TIMEOUT_DEFAULT))
                .thenReturn(12L);
        config = new DefaultAutoUnapproveConfig(applicationPropertiesService, storageService);

        assertEquals(Duration.ofSeconds(12), config.getPatchIdTimeout());
    }

    @Test
    public void testGetTempDir() {
        File tempDir = config.getTempDir();
        assertTrue(tempDir.isDirectory());
        assertEquals("unapprove", tempDir.getName());
        assertEquals(new File(folder.getRoot().toFile(), "unapprove").getAbsolutePath(), tempDir.getAbsolutePath());
    }
}
