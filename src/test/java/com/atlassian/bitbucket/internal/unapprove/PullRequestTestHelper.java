package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestRef;
import com.atlassian.bitbucket.repository.Repository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public final class PullRequestTestHelper {

    private PullRequestTestHelper() {
        throw new UnsupportedOperationException(getClass().getName() + " is a utility class and should not be instantiated");
    }

    public static PullRequest mockPullRequest(Repository repository, String fromCommit, String toCommit) {
        return mockPullRequest(repository, fromCommit, repository, toCommit);
    }

    public static PullRequest mockPullRequest(Repository fromRepository, String fromCommit,
                                              Repository toRepository, String toCommit) {
        PullRequestRef fromRef = mockPullRequestRef(fromRepository, fromCommit);
        PullRequestRef toRef = mockPullRequestRef(toRepository, toCommit);

        PullRequest pullRequest = mock(PullRequest.class);
        when(pullRequest.getFromRef()).thenReturn(fromRef);
        when(pullRequest.getToRef()).thenReturn(toRef);
        when(pullRequest.isCrossRepository()).thenReturn(fromRepository != toRepository); // Reference equality

        return pullRequest;
    }

    public static PullRequestRef mockPullRequestRef(Repository repository, String latestCommit) {
        PullRequestRef pullRequestRef = mock(PullRequestRef.class);
        when(pullRequestRef.getLatestCommit()).thenReturn(latestCommit);
        when(pullRequestRef.getRepository()).thenReturn(repository);

        return pullRequestRef;
    }
}
