package it.com.atlassian.bitbucket.internal.unapprove.ui.page;

import com.atlassian.aui.auipageobjects.AuiCheckbox;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.test.DefaultFuncTestData;
import com.atlassian.pageobjects.DelayedBinder;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.PageBindingWaitException;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.webdriver.bitbucket.page.AbstractPage;
import com.atlassian.webdriver.bitbucket.page.BitbucketPage;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.utils.URIBuilder;
import org.openqa.selenium.NoSuchElementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;

/**
 * This class was created in the plugin codebase instead of using the BitbucketLoginPage from bitbucket-page-objects
 * because it needs to work with Bitbucket Server 7.6 and earlier, which use pre AUIv8.7 versions and thus does not need
 * {@link AuiCheckbox}, while Bitbucket Server 7.7+ (when AUI was upgrade to 8.7) requires the use of {@link AuiCheckbox}.
 */
public class BitbucketLoginPage extends BitbucketPage implements LoginPage {

    private static final Logger log = LoggerFactory.getLogger(BitbucketLoginPage.class);

    private final boolean overrideNextParam;
    private final boolean embedded;
    private final String permission;

    @ElementBy(name = "j_password")
    private PageElement passwordField;
    @ElementBy(name = "submit")
    private PageElement submitButton;

    /**
     * The presence of a "j_username" field is used in some tests to decide whether we have correctly landed on the /login page, or
     * have been redirected to /projects (because we are already logged in).
     */
    @ElementBy(name = "j_username")
    private PageElement usernameField;

    @ElementBy(name = "_atl_remember_me")
    private AuiCheckbox rememberMeCheckbox;
    @ElementBy(id = "captcha")
    private PageElement captcha;
    @ElementBy(id = "captcha-image")
    private PageElement captchaImage;
    @ElementBy(id = "captcha-reload")
    private PageElement captchaReloadButton;
    @ElementBy(name = "permission")
    private PageElement permissionField;

    public BitbucketLoginPage() {
        this(true);
    }

    public BitbucketLoginPage(final boolean overrideNextParam) {
        this(overrideNextParam, false, (String) null);
    }

    public BitbucketLoginPage(final boolean overrideNextParam, boolean embedded, Permission permission) {
        this(overrideNextParam, embedded, permission.name());
    }

    public BitbucketLoginPage(final boolean overrideNextParam, boolean embedded, String permission) {
        this.overrideNextParam = overrideNextParam;
        this.embedded = embedded;
        this.permission = permission;
    }

    @Override
    public String getUrl() {
        try {
            URIBuilder uri = new URIBuilder("/login");
            if (embedded) {
                uri.addParameter("embedded", Boolean.TRUE.toString());
            }
            if (permission != null) {
                uri.addParameter("permission", permission);
            }

            return uri.build().toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isEmbedded() {
        return body.hasClass("user-login-embedded");
    }

    @Override
    public <M extends Page> M login(final String username, final String password, final Class<M> nextPage) {
        return login(username, password, false, nextPage);
    }

    public <M extends Page> M login(final String username, final String password, final Class<M> nextPage, Object... args) {
        return login(username, password, false, nextPage, args);
    }

    public <M extends Page> M login(final String username, final String password, final boolean rememberMe, final Class<M> nextPage, Object... args) {
        for (int i = 1;; i++) {
            try {
                usernameField.clear();
                usernameField.type(username);
            } catch (NoSuchElementException e) {
                DelayedBinder<M> delayedBinder = pageBinder.delayedBind(nextPage, args);
                if (driver.getCurrentUrl().contains(delayedBinder.get().getUrl())) {
                    // we're probably there already - race condition while waiting for a previous login attempt to load the next page we timed out.
                    // Let's run with it.
                    return bind(delayedBinder);
                }
                throw e;
            }
            try {
                return loginWithUserPreFilled(password, rememberMe, nextPage, args);
            } catch (PageBindingWaitException e) {
                if (i >= 3) { // we tried 3 times now. Bail.
                    throw e;
                }
                // Ignore
                log.info("------------------------>>>> Attempt {} to login failed <<<<--------------------------", i, e);
            }
        }
    }

    public <M extends Page> M loginWithUserPreFilled(final String password, final boolean rememberMe, final Class<M> nextPage, Object... args) {
        passwordField.type(password);
        setRememberMe(rememberMe);
        final DelayedBinder<M> delayedBinder = pageBinder.delayedBind(nextPage, args);
        if (!HomePage.class.isAssignableFrom(nextPage) && overrideNextParam) {
            // Don't judge me
            javascriptExecutor.executeScript(
                    "(function() {" +
                    "var $form = jQuery('form.aui')," +
                    "$next = $form.find('input[name=\"next\"]');" +
                    "if ($next.length) { $next.remove(); } else { $next = jQuery('<input name=\"next\" type=\"hidden\">'); }" +
                    "$next.val('" + StringEscapeUtils.escapeEcmaScript(delayedBinder.get().getUrl()) + "');" +
                    "$next.appendTo($form);" +
                    "})()"
            );
        }
        submitButton.click();
        return bind(delayedBinder);
    }

    private <M extends Page> M bind(DelayedBinder<M> delayedBinder) {
        M page = delayedBinder.bind();
        if (page instanceof AbstractPage) {
            AbstractPage abstractPage = (AbstractPage) page;
            Poller.waitUntilTrue(abstractPage.isHereTimed());
        }
        return page;
    }

    @Override
    public <M extends Page> M loginAsSysAdmin(final Class<M> nextPage) {
        return login(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword(), nextPage);
    }

    public <M extends Page> M loginAsSysAdmin(final Class<M> nextPage, Object... args) {
        return login(DefaultFuncTestData.getAdminUser(), DefaultFuncTestData.getAdminPassword(), nextPage, args);
    }

    public <M extends Page> M loginAsRegularUser(final Class<M> nextPage, Object... args) {
        return login(DefaultFuncTestData.getRegularUser(), DefaultFuncTestData.getRegularUserPassword(), nextPage, args);
    }

    private void setRememberMe(boolean rememberMe) {
        // only works on non-embedded login
        if (!isEmbedded()) {
            Poller.waitUntilTrue(rememberMeCheckbox.timed().isVisible());
            if (rememberMe) {
                rememberMeCheckbox.check();
            } else {
                rememberMeCheckbox.uncheck();
            }
        }
    }
}
