package com.atlassian.bitbucket.internal.unapprove;

import javax.annotation.Nonnull;
import java.io.File;
import java.time.Duration;

/**
 * @since 2.2
 */
public interface AutoUnapproveConfig {

    /**
     * @return The maximum time to spend calculating the patch ID
     * @since 4.2
     */
    @Nonnull
    Duration getPatchIdTimeout();

    /**
     * Retrieves the directory where the plugin should store any temporary files it creates.
     *
     * @return the directory for temporary files
     */
    @Nonnull
    File getTempDir();

    /**
     * @return Whether patchId has been disabled globally with a property
     * @since 4.2
     */
    boolean isPatchIdEnabled();
}
