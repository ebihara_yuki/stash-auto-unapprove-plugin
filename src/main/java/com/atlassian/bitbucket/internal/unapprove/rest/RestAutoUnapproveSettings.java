package com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.rest.RestMapEntity;
import com.atlassian.bitbucket.rest.annotation.JsonSurrogate;
import com.atlassian.bitbucket.rest.scope.RestScope;
import com.atlassian.bitbucket.unapprove.AutoUnapproveSettings;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nullable;

@JsonSerialize
@JsonSurrogate(AutoUnapproveSettings.class)
public class RestAutoUnapproveSettings extends RestMapEntity {

    private static final String SCOPE = "scope";
    private static final String ENABLED = "enabled";

    @SuppressWarnings("unused") //Required for Jersey
    public RestAutoUnapproveSettings() {

    }

    public RestAutoUnapproveSettings(AutoUnapproveSettings settings) {
        put(SCOPE, new RestScope(settings.getScope()));
        put(ENABLED, settings.isEnabled());
    }

    @Nullable
    public RestScope getScope() {
        return RestScope.valueOf(get(SCOPE));
    }

    public boolean enabled() {
        return getBoolProperty(ENABLED);
    }
}
