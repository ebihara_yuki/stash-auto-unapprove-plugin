package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.server.StorageService;
import com.atlassian.bitbucket.util.MoreFiles;

import javax.annotation.Nonnull;
import java.io.File;
import java.time.Duration;

/**
 * @since 2.2
 */
public class DefaultAutoUnapproveConfig implements AutoUnapproveConfig {

    static final boolean PATCH_ID_ENABLED_DEFAULT = true;
    static final long PATCH_ID_TIMEOUT_DEFAULT = 20;
    static final String PROP_PATCH_ID_ENABLED = "plugin.bitbucket-pr-auto-unapprove.gitrescopeanalyzer.patchid.enabled";
    static final String PROP_PATCH_ID_TIMEOUT_SECONDS = "plugin.bitbucket-pr-auto-unapprove.gitrescopeanalyzer.patchid.timeout.seconds";

    private final boolean isPatchIdEnabled;
    private final long patchIdTimeoutSeconds;
    private final File tempDir;

    public DefaultAutoUnapproveConfig(ApplicationPropertiesService applicationPropertiesService,
                                      StorageService storageService) {
        tempDir = MoreFiles.mkdir(storageService.getTempDir(), "unapprove").toFile();
        patchIdTimeoutSeconds = Math.max(applicationPropertiesService.getPluginProperty(PROP_PATCH_ID_TIMEOUT_SECONDS,
                PATCH_ID_TIMEOUT_DEFAULT), 0);
        isPatchIdEnabled = applicationPropertiesService.getPluginProperty(PROP_PATCH_ID_ENABLED,
                PATCH_ID_ENABLED_DEFAULT) && patchIdTimeoutSeconds > 0;
    }

    @Nonnull
    @Override
    public Duration getPatchIdTimeout() {
        return Duration.ofSeconds(patchIdTimeoutSeconds);
    }

    @Nonnull
    @Override
    public File getTempDir() {
        return tempDir;
    }

    @Override
    public boolean isPatchIdEnabled() {
        return isPatchIdEnabled;
    }
}
