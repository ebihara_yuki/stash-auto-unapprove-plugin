package com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.rest.util.ResourcePatterns;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Path;

@AnonymousAllowed
@Path(ResourcePatterns.REPOSITORY_URI + "/settings")
@Singleton
public class AutoUnapproveRepositorySettingsResource extends AbstractAutoUnapproveSettingsResource {

    public AutoUnapproveRepositorySettingsResource(AutoUnapproveService unapproveService) {
        super(unapproveService);
    }
}
