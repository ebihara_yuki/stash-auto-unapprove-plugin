package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.audit.AuditEntry;
import com.atlassian.bitbucket.audit.AuditEntryBuilder;
import com.atlassian.bitbucket.event.ApplicationEvent;
import com.atlassian.bitbucket.util.AuditUtils;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;

/**
 * Helper class for converting an {@link AutoUnapproveSettingsChangedEvent} to an {@link AuditEntry} for inclusion in
 * the audit log.
 * <p>
 * This class appears in the API because it is attached to an annotation on {@link AutoUnapproveSettingsChangedEvent}.
 * It is <i>not</i> considered part of the API, however, and offers no compatibility guarantees. Plugin developers
 * should not use this class.
 */
final class AutoUnapproveAuditUtils {

    private AutoUnapproveAuditUtils() {
        throw new UnsupportedOperationException("AutoUnapproveAuditUtils is a constants class and cannot be instantiated");
    }

    static AuditEntryBuilder addCommonAuditDetails(AuditEntryBuilder builder, ApplicationEvent event,
                                                   boolean isEnabled, String target) {
        ImmutableMap<String, Object> details = ImmutableMap.of("enabled", isEnabled);

        String detailsJson;
        try {
            detailsJson = AuditUtils.toJSONString(details);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to convert map %s to JSON", details), e);
        }

        return builder.action(event.getClass())
                .details(detailsJson)
                .target(target)
                .timestamp(event.getDate())
                .user(event.getUser());
    }
}
