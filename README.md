Bitbucket Server Auto Unapprove Pull Request Plugin
===================================================

Adds a pull request setting automatically marking reviewers as unapproved when a pull request is updated.

Additional settings:

* Ability to enable/disable auto-unapprove through the UI
    * At the global level to configure it for all repositories by default
      (`https://<bitbucket-url>/plugins/servlet/autounapprove`)
    * Override the global setting at the project level to configure it for all repositories in that project by default
      (`https://<bitbucket-url>/plugins/servlet/autounapprove/projects/<project-key>`)
    * Override the project setting at the repository level to configure it for that repository
      (`https://<bitbucket-url>/plugins/servlet/autounapprove/projects/<project-key>/repos/<repo-slug>`)
* Patch ID calculation: Will only reset approvals if the diff has changed (e.g. it won't reset approvals for a merge
  commit that doesn't change the diff, or amending a commit message). This behaviour is enabled by default
    * Patch ID can be disabled globally by setting
      `plugin.bitbucket-pr-auto-unapprove.gitrescopeanalyzer.patchid.enabled` in the
      Bitbucket properties file.
    * Patch ID can be expensive to compute. By default, it will timeout after 20 seconds and reset the approvals, but
      this behaviour can also be configured globally by setting
      `plugin.bitbucket-pr-auto-unapprove.gitrescopeanalyzer.patchid.timeout.seconds`

Minimum Requirements
--------------------

* Java 8
* Apache Maven 3.5+
* Atlassian Plugin SDK 8


Building the plugin
-------------------

To build it using the Atlassian Plugin SDK

    atlas-package


Running the plugin in Bitbucket Server
--------------------------------------

To run the plugin using Bitbucket Server in debug mode

    atlas-debug


Running the tests
-----------------

To run the unit tests

    atlas-unit-test
    

Quick Reload
------------

For fast plugin reloads during development, the [Quick Reload](https://bitbucket.org/atlassianlabs/quickreload)
plugin is used. After starting Bitbucket Server with `atlas-debug` or `mvn bitbucket:debug` quick reload can be triggered
by running `atlas-package` or `mvn package` in another terminal window. Quick Reload will detect the updated JAR file
and upload it to the running Bitbucket Server instance.

Quick Reload by default is rather silent and it is not obvious from the logs when the reload has actually completed.
Enabling INFO level logging for the Quick Reload loggers can be done like so:

    curl -u admin -v -X PUT -d "" -H "Content-Type: application/json" \
    http://localhost:7990/bitbucket/rest/api/latest/logs/logger/com.atlassian.labs.plugins.quickreload/info

After this is enabled, each time a reload completes the message "Quick Reload Finished" will be logged.
